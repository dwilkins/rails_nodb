# Files here

## ingress-prometheus.json

Used to expose access to the Prometheus installed in the minikube cluster

Edit the parts of this file like `prometheus.192.168.64.18.xip.io` to have your
IP or your base URL specified in the cluster


## local.openssl.cnf 

Used as input to openssl when creating a certificate with IP SANs (Subject
Alternative Names).  When using TLS with an IP Address, the container registry
will complain that the certificate does not contain IP SANs