# Local development environment with Minikube
* Autodevops
* Gitlab Container Registry
* Prometheus monitoring
* ...

** This won't build on GitLab.com **

## Assumptions
* GDK is working
* Minikube is using the hyperkit vm-driver.  If VirtualBox driver, then IP 192.168.64.1 should be 192.168.99.1
* Minikube installed (1.4.0 w/ k8s 1.15.4 - other versions may work)
* Minikube bridge IP address is 192.168.64.1

## Helpful minikube configuration defaults
* `minikube config set kubernetes-version v1.15.4`
* `minikube config set vm-driver hyperkit`  # Mac only
* `minikube config set memory 3000m`

## Steps
* Add 192.168.64.1:5443 as an insecure registry to Docker Desktop (on mac - need Linux instructions)
* `gdk init gdk-minikube`
* `cd gdk-minikube`
* `gdk install` :coffee:
* copy the file local.openssl.cnf into this directory from here https://gitlab.com/dwilkins/rails_nodb/raw/master/minikube-assets/local.openssl.cnf
* `openssl req  -config ./local.openssl.cnf -subj "/CN=localhost/" -days 365 -x509 -newkey rsa:2048 -sha256 -nodes -keyout localhost.key -out localhost.crt -outform PEM`
* `mkdir -p ~/.minikube/files/etc/ssl/certs`
* `cp localhost.crt ~/.minikube/files/etc/ssl/certs/gdk-minikube.pem` 
* Trust the gdk-minikube/localhost.crt in the OS
* `minikube start -p gdk-minikube --kubernetes-version=v1.15.4`
* `minikube addons enable ingress -p gdk-minikube`
* `echo 0.0.0.0 > host`
* `echo true > registry_enabled`
* `gdk reconfigure`
* edit gitlab/config/gitlab.yml changing keys:
  * production->gitlab->host to 192.168.64.1
  * production->registry->enabled to true
  * production->registry->host to 192.168.64.1
  * production->registry->port to 5443
* edit registry/config.yml
  * auth->token->realm to http://192.168.64.1:3000/jwt/auth
* edit nginx/conf/nginx.conf changing the file
  * add below the `upstream gitlab-workhorse` section:
```    
  upstream gitlab-registry {
    server 0.0.0.0:5000 fail_timeout=0;
  }
```
  * add after the existing `server` section:
```
  server {
    listen 192.168.64.1:5443 ssl;

    ssl_certificate PATH_TO_YOUR_GDK/gdk-minikube/localhost.crt;
    ssl_certificate_key PATH_TO_YOUR_GDK/gdk-minikube/localhost.key;

    location / {
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto   $scheme;
        proxy_set_header    Upgrade             $http_upgrade;
        proxy_set_header    Connection          $connection_upgrade;

        proxy_read_timeout 300;

        proxy_pass http://gitlab-registry;
    }
  }
```
* uncomment the `nginx` line in the Procfile
* `gdk run`
* Navigate to `http://localhost:3000/` and login
* Navigate to `http://localhost:3000/admin/application_settings/network`
  * check `Allow requests to the local network from web hooks and services`
* Navigate to `http://localhost:3000/admin/clusters`
* Get the cluster IP with `minikube ip -p gdk-minikube`
* Add the rbac stuff 
  * `kubectl create clusterrolebinding permissive-binding   --clusterrole=cluster-admin   --user=admin   --user=kubelet   --group=system:serviceaccounts`
* Start the minikube dashboard with `minikube dashboard -p gdk-minikube`
* Add the cluster to the instance using the IP address, ca.cert and token from the Minikube dashboard
  * Be sure to Uncheck the `RBAC-enabled cluster` checkbox
* Add a base domain to your cluster like MINIKUBE_IP.xip.io (192.168.64.22.xip.io)
* Install Helm
* Install Prometheus
* Instal GitLab Runner
* Import the project (using Git URL) from https://gitlab.com/dwilkins/rails_nodb.git
* Navigate to http://localhost:3000/root/rails_nodb/-/settings/ci_cd
* Add a variable called `K8S_SECRET_RAILS_MASTER_KEY` with the value `7ea0573e71a231760b3ea9216d57a104`
* Navigate to the CI/CD Piplelines page and "Run Pipeline"
* PROFIT!
* Should be able to navigate to https://root-rails-nodb.MINIKUBE_IP.xip.io


## Notes

* Sometimes `minikube` gets out of sync with the correct date / time. This can be fixed with:
  `minikube ssh -p gdk-minikube -- docker run -i --rm --privileged --pid=host debian nsenter -t 1 -m -u -n -i date -u $(date -u +%m%d%H%M%Y)`
* use the file (ingress-prometheus.json)[https://gitlab.com/dwilkins/rails_nodb/blob/master/minikube-assets/ingress-prometheus.json]
  to expose your prometheus as `https://prometheus.MINIKUBE_IP.xip.io/` 
  kubectl create -f ingress-prometheus.json
