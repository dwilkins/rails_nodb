FROM ruby:2.6.3
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash \
    && apt-get update -qq && \
    apt-get install nodejs -yq && \
    npm install yarn -g

RUN gem install bundler
# xoRUN bundle install
RUN mkdir /rails_nodb
COPY Gemfile /rails_nodb/Gemfile
COPY Gemfile.lock /rails_nodb/Gemfile.lock
RUN cd /rails_nodb && bundle install
COPY . /rails_nodb
RUN chmod +x /rails_nodb/run.sh
WORKDIR /rails_nodb

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 5000
ENV PORT 5000

# Start the main process.
# CMD ["rails", "server", "-b", "0.0.0.0", "-p", "5000","-e", "production"]
CMD ["./run.sh"]